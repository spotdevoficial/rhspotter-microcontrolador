/////////////////////////////////////////////////////////////////////
//
//   _____  _    _  _____             _   _            
//  |  __ \| |  | |/ ____|           | | | |           
//  | |__) | |__| | (___  _ __   ___ | |_| |_ ___ _ __ 
//  |  _  /|  __  |\___ \| '_ \ / _ \| __| __/ _ \ '__|
//  | | \ \| |  | |____) | |_) | (_) | |_| ||  __/ |   
//  |_|  \_\_|  |_|_____/| .__/ \___/ \__|\__\___|_|   
//                       | |                           
//                       |_|                           
//
//
//                                      Autor: Davi Salles
//                                      Descrição: Script para 
//                                      controle do robô RHSpotter
//
/////////////////////////////////////////////////////////////////////

#include <Servo.h>

/*************************************
 * 
 *        C O N S T A N T E S
 * 
 *************************************/

// Classe para armazenar as constantes dos pinos
class Pino {
  public:
     // Motores de movimento do robô
     static const int motorFrontalDireita = 5;          // Motor da parte da frente a direita do robô
     static const int motorFrontalEsquerda = 6;         // Motor da parte da frente a esquerda do robô
     static const int motorTraseiraEsquerda = 9;        // Motor da parte de trás a direita do robô
     static const int motorTraseiraDireita = 3;         // Motor da parte de trás a direita do robô;
  
     // Motor de movimento da câmera        
     static const int motorCamera = 0;                   // Motor para o movimento da camera  
};

/*************************************
 * 
 *    VARIAVEIS DO SISTEMA
 * 
 *************************************/

// Motores de movimento do robô
Servo frontalDireita;          // Motor da parte da frente a direita do robô
Servo frontalEsquerda;         // Motor da parte da frente a esquerda do robô
Servo traseiraEsquerda;        // Motor da parte de trás a direita do robô
Servo traseiraDireita;         // Motor da parte de trás a direita do robô;
Servo camera;                   // Motor para o movimento da camera  


/*************************************
 * 
 *   INICIALIZAÇÃO DO SISTEMA
 * 
 *************************************/

void setup() {

  // Configura os pinus para output
  pinMode(Pino::motorFrontalDireita, OUTPUT);
  digitalWrite(Pino::motorFrontalDireita, LOW);
  
  pinMode(Pino::motorFrontalEsquerda, OUTPUT);
  digitalWrite(Pino::motorFrontalEsquerda, LOW);
  
  pinMode(Pino::motorTraseiraDireita, OUTPUT);
  digitalWrite(Pino::motorTraseiraDireita, LOW);
  
  pinMode(Pino::motorTraseiraEsquerda, OUTPUT);
  digitalWrite(Pino::motorTraseiraDireita, LOW);

  // Configura a comunicação serial
  Serial.begin(9600);
  
  // Configura os servos
  frontalDireita.attach(Pino::motorFrontalDireita);
  frontalEsquerda.attach(Pino::motorFrontalEsquerda);
  traseiraDireita.attach(Pino::motorTraseiraDireita);
  traseiraEsquerda.attach(Pino::motorTraseiraEsquerda); 

}

/*************************************
 * 
 *        LOOP POR TICK 
 * 
 *************************************/

void loop() {

  String inString = "";
  while (Serial.available() > 0) {

      String lida = Serial.readString();
      if(lida == "frente"){
        traseiraDireita.write(180); 
        traseiraEsquerda.write(0); 
        frontalDireita.write(180); 
        frontalEsquerda.write(0); 
      }else if(lida == "atras"){
        traseiraDireita.write(0); 
        traseiraEsquerda.write(180); 
        frontalDireita.write(0); 
        frontalEsquerda.write(180); 
      }else if(lida == "parar"){
        traseiraDireita.write(90); 
        traseiraEsquerda.write(90); 
        frontalDireita.write(90); 
        frontalEsquerda.write(90);
      }
    
  }

}

